// Base server setup
var express 		= require('express');
var mongoose 		= require('mongoose');
var bodyParser		= require('body-parser');
var app				= express();
var port			= 1337;

mongoose.connect('mongodb://localhost/simple');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.static(__dirname + '/app'));

app.get('/', function(req, res) {
	res.sendfile('./app/index.html');
});

// Lets build a router
var router 			= express.Router();

var Paste			= require('./models/paste');

router.get('/', function(req, res) {
	res.json({ message: 'Get lost' });
});

router.route('/pastes')
	.post(function(req, res) {
		var paste = new Paste();
		paste.header 	= req.body.header;
		paste.body		= req.body.body;

		paste.save(function(err, product) {
			if(err)
				res.send(err);
			res.json(product)
		});
	})
	.get(function(req, res) {
		Paste.find(function(err, pastes) {
			if(err)
				res.send(err);
			res.json(pastes);
		})
	});

router.route('/paste/:paste_id')
	.get(function(req, res) {
		Paste.findById(req.params.paste_id, function (err, paste) {
			if(err)
				res.send(err);

			res.json(paste);
		})
	})
	.delete(function(req, res) {
		Paste.remove({
			_id: req.params.paste_id
		}, function(err, paste) {
			if(err)
				res.send(err);

			res.json({ message: 'I deleted this paste!' });
		});
	});

app.use('/api', router);

// Start our server
app.listen(port);
console.log('We are up and running on: ' + port);