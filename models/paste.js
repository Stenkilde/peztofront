var mongoose			= require('mongoose');
var Schema				= mongoose.Schema;

var PasteSchema = new Schema({
	header: String,
	body: String
});

module.exports = mongoose.model('Paste', PasteSchema);